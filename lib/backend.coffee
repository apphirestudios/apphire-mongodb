db = undefined
monk = require 'monk'
async = require 'co'
helpers = require 'apphire-helpers'

debug = (msg)->
  return
  console.log msg

module.exports = mongo = (connectionString)->
  db = monk(connectionString)
  conn =
    get: (name)->
      c = db.get(name)
      Collection = {}
      allMethods.map (methodName)->
        Collection[methodName] = (query, rest...)-> async =>
          if transformQueryMethods.indexOf(methodName) > -1
            transformQuery(query)
          rest ?= []
          if query
            args = [query].concat(rest)
          else
            args = arguments
          debug 'ARGS: ' + JSON.stringify(args)
          debug 'METHOD: ' + methodName
          resp = yield c[methodName].apply c[methodName], args
          if transformResponseMethods.indexOf(methodName) > -1
            transformResponse(resp)
          debug 'RESP: ' + JSON.stringify(resp)
          return resp

      Collection.drop = c.drop.bind c
      return Collection

    close: db.close.bind db
    create: db.create.bind db

  return conn

mongo.id = monk.id.bind monk

transformQuery = (query)->
  return if not query?
  if query.id
    query._id = query.id
    delete query.id

stringifyObjectId = (rec)->
  return if not rec?
  return if not helpers.isObject(rec)
  for a of rec
    if rec[a]?.constructor?.name is 'ObjectID'
      rec[a] = rec[a].toString()
    stringifyObjectId(rec[a])

transformResponseRecord = (rec)->
  stringifyObjectId(rec)
  if rec._id
    rec.id = rec._id
    delete rec._id

transformResponse = (response)->
  return if not response?

  if (response instanceof Array)
    for elem in response
      transformResponseRecord elem
  else
      transformResponseRecord response



allMethods = ['find', 'findOne', 'findOneAndUpdate', 'findOneAndDelete', 'remove', 'update', 'insert', 'count', 'aggregate', 'group']
transformQueryMethods = ['find', 'findOne', 'findOneAndUpdate', 'findOneAndDelete', 'remove', 'update', 'insert', 'count', 'aggregate']
transformResponseMethods = ['find', 'findOne', 'insert']
collectionMethods = ['drop']

